const express = require('express');
const router = express.Router();
const { secured } = require('../../auth');
const c = require('../../db/constants.js');

const Account = require('../../db/account');
const Budget = require('../../db/budget');
const Payee = require('../../db/payee');
const Currency = require('../../db/currency');
const Category = require('../../db/category');
const Subcategory = require('../../db/subcategory');
const Transaction = require('../../db/transaction');

router.get('/init', secured, (req, res) => {
    const data = {
        user: req.user,
        accounts: null,
        budgets: null,
        payees: null,
        currencies: null,
        categories: null,
        subcategories: null,
        transactions: null,
    };

    const accounts = Account.find({ user: req.user.id });
    const currencies = Currency.getByUser(req.user.id);
    const budgets = Budget.find({ user: req.user.id });
    const payees = Payee.getByUser(req.user.id);
    const categories = Category.getByBudget(req.user.selectedBudget);
    const subcategories = Subcategory.getByBudget(req.user.selectedBudget);
    const transactions = Transaction.getByUser(req.user.id);

    Promise.all([
        accounts,
        currencies,
        budgets,
        payees,
        categories,
        subcategories,
        transactions,
    ]).then(([
        accounts,
        currencies,
        budgets,
        payees,
        categories,
        subcategories,
        transactions,
    ]) => {
        data.accounts = accounts;
        data.budgets = budgets;
        data.payees = payees;
        data.transactions = transactions;
        data.categories = categories;
        data.subcategories = subcategories;
        data.currencies = currencies;

        res.json(data);
    });
});

router.post('/accounts', secured, (req, res) => {
    const account = new Account(req.body);

    account.user = req.user.id;
    account.type = c.ACCOUNT_TYPE_SAVINGS;

    account.save().then(() => res.json(account));
});

router.put('/accounts/:id', secured, (req, res) => {
    Account
        .findById(req.params.id)
        .then(account => {
            account.name = req.body.name;
            account.balances = req.body.balances;
            account.save().then(() => res.json(account));
        });
});

router.post('/transaction', secured, (req, res) => {

});

module.exports = router;
