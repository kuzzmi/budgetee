const mongoose = require('mongoose');

const <%= pascalEntityName %>Schema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
<%= pascalEntityName %>Schema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
<%= pascalEntityName %>Schema.methods = {
};

<%= pascalEntityName %>Schema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('<%= pascalEntityName %>', <%= pascalEntityName %>Schema);
