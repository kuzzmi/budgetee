const mongoose = require('mongoose');
const { validateMonth, fromDateToMonth } = require('../../utils/date.js');

const BalanceSchema = new mongoose.Schema({
    value: {
        type: Number,
        default: 0,
        required: true,
    },
    activity: {
        type: Number,
        default: 0,
    },
    currency: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Currency',
        required: true,
    },
});

const MonthlyBudgetSchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    month: {
        type: String,
        default: () => fromDateToMonth(new Date()),
        validate: [
            validateMonth,
            'Monthly budget month is not valid',
        ],
    },

    balances: {
        type: [ BalanceSchema ],
        required: true,
        validate: [
            val => val.length > 0,
            'Monthly budget has to have at least one balance value and currency',
        ],
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
MonthlyBudgetSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
MonthlyBudgetSchema.methods = {
};

MonthlyBudgetSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('MonthlyBudget', MonthlyBudgetSchema);
module.exports.Schema = MonthlyBudgetSchema;
