const mongoose = require('mongoose');

const BudgetSchema = new mongoose.Schema({
    __version: {
        type: String,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    description: {
        type: String,
        required: false,
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
BudgetSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
BudgetSchema.methods = {
};

BudgetSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Budget', BudgetSchema);
