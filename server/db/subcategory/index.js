const mongoose = require('mongoose');
const Budget = require('../budget');
const Category = require('../category');
const Account = require('../account');
const MonthlyBudgetSchema = require('../monthlyBudget').Schema;

const { uniq, groupBy } = require('ramda');

const SubcategorySchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    description: {
        type: String,
        required: false,
    },

    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
    },

    monthlyBudgets: {
        type: [ MonthlyBudgetSchema ],
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

// SubcategorySchema
//     .post('save', function(doc, next) {
//         Category.findById(doc.category)
//             .then(({ budget }) => Budget.findById(category.budget))
//             .then(({ user }) => {
//                 return Promise.all([
//                     Currency.getByUser(budget.user),
//                     User.findById(user)
//                 ]);
//             })
//             .then([ currencies, user ] => {
//                 const monthlyBudget = {
//                     month: user.selectedMonth,
//                     balances: currencies.map(c => {
//                         value: 0,
//                         currency: c.id,
//                     }),
//                 };
//             });
//     });

/**
 * Virtuals
 */
SubcategorySchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
SubcategorySchema.methods = {
    adjustMonthlyBalanceActivity: function(month, value, currency) {
        this.monthlyBudgets = this.monthlyBudgets.map(mb => {
            if (mb.month === month) {
                mb.balances = mb.balances.map(b => {
                    if (b.currency.toString() === currency.toString()) {
                        b.activity = b.activity + value;
                        return b;
                    }
                    return b;
                });
                return mb;
            } else {
                return mb;
            }
        });

        return this.save();
    },
};

/**
 * Statics
 */
SubcategorySchema.static('getByBudget', function(budgetId) {
    // get all accounts by user first
    return Category
        .getByBudget(budgetId)
        .then(categories => {
            const query = this.find({
                category: {
                    $in: categories.map(a => a.id),
                },
            });
            return query.exec();
        });
});

SubcategorySchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Subcategory', SubcategorySchema);
