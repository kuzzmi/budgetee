const mongoose = require('mongoose');

const GoalSchema = new mongoose.Schema({
    __version: {
        type: String,
        default: 1,
    },
});

/**
 * Virtuals
 */
GoalSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
GoalSchema.methods = {
};

GoalSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Goal', GoalSchema);
