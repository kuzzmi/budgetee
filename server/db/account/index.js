const mongoose = require('mongoose');
const c = require('../constants.js');

const AccountSchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    description: {
        type: String,
        required: false,
        default: '',
    },

    type: {
        type: Number,
        enum: [
            c.ACCOUNT_TYPE_TRACKING,
            c.ACCOUNT_TYPE_SAVINGS,
            c.ACCOUNT_TYPE_DEBT,
        ],
        required: true,
    },

    balances: {
        type: [{
            value: {
                type: Number,
                default: 0,
                required: true,
            },
            currency: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Currency',
                required: true,
            },
        }],
        required: true,
        validate: [
            val => val.length > 0,
            'Account has to have at least one balance currency',
        ],
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
AccountSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
AccountSchema.methods = {
    updateBalance: function(value, currency) {
        this.balances = this.balances.map(balance => {
            if (balance.currency.toHexString() === currency) {
                balance.value += value;
            }
            return balance;
        });

        return this.save().then(() => this);
    },
};

/**
 * Statics
 */
AccountSchema.static('getByUser', function(userId) {
    return this.find({ user: userId });
});

AccountSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Account', AccountSchema);
