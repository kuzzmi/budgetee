const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    budget: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Budget',
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
CategorySchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Statics
 */
CategorySchema.static('getByBudget', function(budgetId) {
    return this.find({ budget: budgetId });
});

/**
 * Methods
 */
CategorySchema.methods = {
};

CategorySchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Category', CategorySchema);
