const mongoose = require('mongoose');
const Account = require('../account');
const c = require('../constants.js');
const Subcategory = require('../subcategory');

const { fromDateToMonth } = require('../../utils/date.js');

const TransactionSchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    transactionSchedule: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TransactionSchedule',
        required: true,
    },

    subcategory: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subcategory',
        required: true,
    },

    account: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        required: true,
    },

    payee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payee',
        required: true,
    },

    name: {
        type: String,
        required: true,
    },

    type: {
        type: Number,
        enum: [
            c.TRANSACTION_TYPE_INCOME,
            c.TRANSACTION_TYPE_EXPENSE,
            c.TRANSACTION_TYPE_TRANSFER,
        ],
        required: true,
    },

    value: {
        type: Number,
        default: 0,
        required: true,
    },

    currency: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Currency',
        required: true,
    },

    cleared: {
        type: Boolean,
        default: false,
        required: true,
    },

    approved: {
        type: Boolean,
        default: false,
        required: true,
    },

    date: {
        type: Date,
        default: Date.now,
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Hooks
 */
TransactionSchema
    .post('save', function(doc, next) {
        Subcategory
            .findById(doc.subcategory)
            .then(sc => {
                return sc.adjustMonthlyBalanceActivity(
                    fromDateToMonth(doc.date),
                    this.value,
                    this.currency,
                ).catch(e => {
                    console.log(e);
                });
            })
            .then(() => {
                Account
                    .findById(doc.account)
                    .then(account => {
                        const currency = doc.currency.toHexString()
                        switch (doc.type) {
                            case c.TRANSACTION_TYPE_INCOME:
                                return account.updateBalance(doc.value, currency);
                            case c.TRANSACTION_TYPE_EXPENSE:
                                return account.updateBalance(-1 * doc.value, currency);
                            default:
                                return account.updateBalance(doc.value, currency);
                        }
                    })
            })
            .then(() => next());
    });

/**
 * Virtuals
 */
TransactionSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
TransactionSchema.methods = {
};

/**
 * Statics
 */
TransactionSchema.static('getByUser', function(userId) {
    // get all accounts by user first
    return Account
        .getByUser(userId)
        .then(accounts => {
            const query = this.find({
                account: {
                    $in: accounts.map(a => a.id),
                },
            });
            return query.exec();
        });
});

TransactionSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Transaction', TransactionSchema);
