const mongoose = require('mongoose');

const BudgetGoalSchema = new mongoose.Schema({
    __version: {
        type: String,
        default: 1,
    },
});

/**
 * Virtuals
 */
BudgetGoalSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
BudgetGoalSchema.methods = {
};

BudgetGoalSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('BudgetGoal', BudgetGoalSchema);
