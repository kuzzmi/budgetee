const mongoose = require('mongoose');
const Transaction = require('../transaction');

const PayeeSchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    synonyms: [ String ],

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
PayeeSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
PayeeSchema.methods = {
};

/**
 * Statics
 */
PayeeSchema.static('getByUser', function(userId) {
    return Transaction
        .getByUser(userId)
        .then(transactions =>
            Transaction
                .distinct('payee')
                .find({
                    _id: {
                        $in: transactions.map(t => t.id),
                    },
                })
                .select('payee')
                .exec()
        )
        .then(transactions =>
            this
                .find({
                    _id: {
                        $in: transactions.map(t => t.payee),
                    },
                })
                .exec()
        );
});

PayeeSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Payee', PayeeSchema);
