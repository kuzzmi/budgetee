const c = require('./constants.js');

const Account = require('./account');
const Budget = require('./budget');
const Category = require('./category');
const Currency = require('./currency');
const Subcategory = require('./subcategory');
const MonthlyBudget = require('./monthlyBudget');
const Transaction = require('./transaction');
const TransactionSchedule = require('./transactionSchedule');
const Payee = require('./payee');
const User = require('./user');

User
    .find()
    .remove()
    .then(() => {
        const user1 = new User({
            name: 'user',
            email: 'email@example.com',
        });

        return Promise.all([
            user1.save(),
            Account.find().remove(),
            Budget.find().remove(),
            Category.find().remove(),
            Currency.find().remove(),
            Subcategory.find().remove(),
            Transaction.find().remove(),
            TransactionSchedule.find().remove(),
            Payee.find().remove(),
        ]);
    })
    .then(([ user ]) => {
        const usd = new Currency({
            name: 'United States Dollar',
            symbol: 'USD',
        });
        const chf = new Currency({
            name: 'Swiss Franc',
            symbol: 'CHF',
        });
        const eur = new Currency({
            name: 'Euro',
            symbol: 'EUR',
        });
        const uah = new Currency({
            name: 'Ukrainian Hryvna',
            symbol: 'UAH',
        });
        const jpy = new Currency({
            name: 'Japanese Yen',
            symbol: 'JPY',
        });

        return Promise.all([
            usd.save(),
            chf.save(),
            uah.save(),
            eur.save(),
            jpy.save(),
            user,
        ]);
    })
    .then(([ usd, chf, uah, eur, _, user ]) => {
        const newAcc1 = new Account({
            name: 'Cash',
            description: '',
            type: c.ACCOUNT_TYPE_TRACKING,
            balances: [{
                value: 540,
                currency: usd.id,
            }, {
                value: 24130,
                currency: chf.id,
            }, {
                value: 35890,
                currency: eur.id,
            }, {
                value: 3890,
                currency: uah.id,
            }],
            user: user.id,
        });

        const newAcc2 = new Account({
            name: 'Current',
            description: '',
            type: c.ACCOUNT_TYPE_TRACKING,
            balances: [{
                value: 345890,
                currency: chf.id,
            }],
            user: user.id,
        });

        const newBudget = new Budget({
            name: 'Budget 1',
            description: '',
            user: user.id,
        });

        return Promise.all([
            newAcc1.save(),
            newAcc2.save(),
            newBudget.save(),
            usd,
            chf,
            uah,
            eur,
            user,
        ]);
    })
    .then(([ account, _, budget, usd, chf, uah, eur, user ]) => {
        user.selectedAccount = account.id;
        user.selectedBudget = budget.id;

        const category1 = new Category({
            name: 'Category 1',
            budget: budget.id,
        });
        const category2 = new Category({
            name: 'Category 2',
            budget: budget.id,
        });

        Promise
            .all([
                category1.save(),
                category2.save(),
                user.save(),
            ])
            .then(([ cat1, cat2 ]) => {
                const subcategory1 = new Subcategory({
                    name: 'Subcategory 1',
                    monthlyBudgets: [{
                        month: '05-2018',
                        balances: [{
                            value: 5000,
                            currency: usd.id,
                        }],
                    }],
                    category: cat1.id,
                });
                const subcategory2 = new Subcategory({
                    name: 'Subcategory 2',
                    monthlyBudgets: [{
                        month: '05-2018',
                        balances: [{
                            value: 5000,
                            currency: uah.id,
                        }, {
                            value: 5000,
                            currency: chf.id,
                        }],
                    }],
                    category: cat1.id,
                });
                const subcategory3 = new Subcategory({
                    name: 'Subcategory 3',
                    monthlyBudgets: [{
                        month: '05-2018',
                        balances: [{
                            value: 5000,
                            currency: uah.id,
                        }, {
                            value: 5000,
                            currency: chf.id,
                        }],
                    }],
                    category: cat2.id,
                });
                const subcategory4 = new Subcategory({
                    name: 'Subcategory 4',
                    monthlyBudgets: [{
                        month: '04-2018',
                        balances: [{
                            value: 5000,
                            currency: uah.id,
                        }, {
                            value: 5000,
                            currency: chf.id,
                        }],
                    }],
                    category: cat2.id,
                });
                const payee1 = new Payee({
                    name: 'Payee1',
                });
                const payee2 = new Payee({
                    name: 'Payee2',
                });

                return Promise.all([
                    subcategory1.save(),
                    subcategory2.save(),
                    subcategory3.save(),
                    subcategory4.save(),
                    payee1.save(),
                    payee2.save(),
                    usd,
                    chf,
                    uah,
                    eur,
                ]);
            }).then(([ sc1, sc2, sc3, sc4, p1, p2, usd, chf, uah, eur ]) => {
                const trSched1 = new TransactionSchedule({
                    frequency: c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE,
                    account: account.id,
                    subcategory: sc1.id,
                    type: c.TRANSACTION_TYPE_EXPENSE,
                    name: 'Bought nothing',
                    value: 500,
                    currency: usd.id,
                    payee: p1.id,
                    date: new Date(),
                });

                const trSched2 = new TransactionSchedule({
                    frequency: c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE,
                    account: account.id,
                    subcategory: sc1.id,
                    type: c.TRANSACTION_TYPE_INCOME,
                    name: 'Earned something',
                    value: 500,
                    currency: eur.id,
                    payee: p1.id,
                    date: new Date(),
                });

                return Promise.all([
                    trSched1.save(),
                    trSched2.save(),
                ]);
            });
    });

