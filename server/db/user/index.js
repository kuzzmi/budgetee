const mongoose = require('mongoose');
const crypto = require('crypto');
const createHash = crypto.createHash;
const { validateMonth, fromDateToMonth } = require('../../utils/date.js');

const UserSchema = new mongoose.Schema({
    __version: {
        type: String,
        default: 1,
    },

    name: {
        type: String,
        required: true,
    },

    email: {
        type: mongoose.SchemaTypes.Email,
        lowercase: true,
        required: true,
        unique: true,
    },

    // invitation testing stuff
    // invitation: {
    //     code: {
    //         type: String,
    //         default: "",
    //         required: false,
    //     },
    //     codeUsed: {
    //         type: Boolean,
    //         default: false,
    //     },
    //     codeSent: {
    //         type: Boolean,
    //         default: false,
    //     },
    // },

    // // authentication stuff
    // hashedPassword: {
    //     type: String,
    //     required: true,
    // },
    //
    // salt: {
    //     type: String,
    //     required: true,
    // },
    //
    // resetPassword: {
    //     token: String,
    //     expires: Date,
    // },
    selectedAccount: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        default: null,
    },

    selectedBudget: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Budget',
        default: null,
    },

    selectedMonth: {
        type: String,
        default: () => fromDateToMonth(new Date()),
        // validate: [
        //     validateMonth,
        //     'Selected month is not valid',
        // ],
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});


/**
 * Virtuals
 */
UserSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

UserSchema
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

// Public profile information
UserSchema
    .virtual('profile')
    .get(function() {
        return {
            name: this.name,
            role: this.role,
        };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
        return {
            'id': this.id,
        };
    });

/**
 * Methods
 */
UserSchema.methods = {
    getInvitationCode: function() {
        if (!this.invitation.code) {
            const { createdAt, _id, email } = this;
            const code = createHash('sha256').update([
                createdAt,
                _id,
                email,
            ].join('.')).digest('hex');
            this.invitation.code = code.toString('base64').replace(/[^A-Za-z\d]/g, '');
        }
        return this.invitation.code;
    },

    validateInvitationCode: function(code) {
        if (this.invitation.codeUsed) {
            return false;
        }
        if (this.getInvitationCode() === code) {
            return true;
        }
        return false;
    },

    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashedPassword;
    },

    makeSalt: function() {
        return crypto.randomBytes(32).toString('base64');
    },

    encryptPassword: function(password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('base64');
    }
};

UserSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('User', UserSchema);
