const mongoose = require('mongoose');
require('mongoose-type-email');

mongoose.Promise = Promise;

const { DB_URL, DB_SEED } = process.env;

mongoose.connect(DB_URL);

if (DB_SEED) {
    require('./seed.js');
}
