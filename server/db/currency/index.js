const mongoose = require('mongoose');
const Account = require('../account');
const User = require('../user');
const { uniq } = require('ramda');

const CurrencySchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    name: {
        type: String,
        required: true,
        unique: true,
    },

    symbol: {
        type: String,
        required: true,
        unique: true,
    },
});

/**
 * Virtuals
 */
CurrencySchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
CurrencySchema.methods = {
};

/**
 * Statics
 */
CurrencySchema.static('getByUser', function(userId) {
    return User.findById(userId).then(user => {
        return Account.getByUser(user.id).then(accounts => {
            const currencies =
                uniq(
                    accounts.reduce((acc, cur) => ([
                        ...acc,
                        ...cur.balances,
                    ]), []).map(b => b.currency)
                );

            return this.find({ _id: {
                $in: currencies,
            }});
        });
    });
});

CurrencySchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Currency', CurrencySchema);
