const mongoose = require('mongoose');
const Transaction = require('../transaction');
const c = require('../constants.js');

const TransactionScheduleSchema = new mongoose.Schema({
    __version: {
        type: Number,
        default: 1,
    },

    frequency: {
        type: Number,
        enum: [
            c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE,
        ],
        default: c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE,
        required: true,
    },

    status: {
        type: Number,
        enum: [
            c.TRANSACTION_SCHEDULE_STATUS_INACTIVE,
            c.TRANSACTION_SCHEDULE_STATUS_ACTIVE,
        ],
        default: c.TRANSACTION_SCHEDULE_STATUS_ACTIVE,
        required: true,
    },

    account: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
    },

    subcategory: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subcategory',
    },

    // Template of a new transaction stuff

    name: {
        type: String,
        required: true,
    },

    type: {
        type: Number,
        enum: [
            c.TRANSACTION_TYPE_INCOME,
            c.TRANSACTION_TYPE_EXPENSE,
            c.TRANSACTION_TYPE_TRANSFER,
        ],
        required: true,
    },

    value: {
        type: Number,
        default: 0,
        required: true,
    },

    currency: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Currency',
        required: true,
    },

    payee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payee',
        required: true,
    },

    date: {
        type: Date,
        default: Date.now,
        required: true,
    },

    createdAt: {
        type: Date,
        default: Date.now,
    },

    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Virtuals
 */
TransactionScheduleSchema
    .virtual('id')
    .get(function() {
        return this._id.toHexString();
    });

/**
 * Methods
 */
TransactionScheduleSchema.methods = {
    execute: function() {
        const transaction = new Transaction({
            name: this.name,
            type: this.type,
            value: this.value,
            date: new Date(),
            currency: this.currency,
            payee: this.payee,
            transactionSchedule: this.id,
            subcategory: this.subcategory,
            account: this.account,
            cleared: false,
            approved: false,
        });

        if (this.frequency === c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE) {
            transaction.approved = true;
        }

        return transaction
                .save()
                .then(() => {

                    // On a successful creation update a schedule status
                    // if frequency is marked as "ONCE"
                    if (this.frequency === c.TRANSACTION_SCHEDULE_FREQUENCY_ONCE) {
                        this.status = c.TRANSACTION_SCHEDULE_STATUS_INACTIVE;

                        return Promise.all([
                            transaction,
                            this.save(),
                        ]);
                    }

                    return [ transaction ];
                })
                .then(([ transaction ]) => transaction);
    },
};

TransactionScheduleSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('TransactionSchedule', TransactionScheduleSchema);
