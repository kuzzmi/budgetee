const { leftpad } = require('./string.js');

module.exports.validateMonth = value => {
    const [ month, year ] = value.split('-');
    const month_ = parseInt(month, 10);
    const year_ = parseInt(year, 10);

    if (!month && !year) {
        return false;
    }

    if (month.length !== 2 || isNaN(month_) || month_ < 1 || month_ > 12) {
        return false;
    }

    if (year.length !== 4 || isNaN(year_)) {
        return false;
    }

    return true;
};

module.exports.fromDateToMonth = date => {
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    return leftpad(month, 2, '0') + '-' + year;
};
