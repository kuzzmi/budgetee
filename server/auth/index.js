const User = require('../db/user');

const user = User.findOne({ name: 'user' });

module.exports.secured = (req, res, next) => {
    user.then(user_ => {
        req.user = user_;
        next();
    }).catch(next);
};

module.exports.withUser = (req, res, next) => {
    user.then(user_ => {
        req.user = user_;
        next();
    }).catch(() => {
        next();
    });
};
