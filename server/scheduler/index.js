const { CronJob } = require('cron');
const TransactionSchedule = require('../db/transactionSchedule');
const c = require('../db/constants.js');

new CronJob('* * * * * *', function() {

    TransactionSchedule
        .find({
            status: c.TRANSACTION_SCHEDULE_STATUS_ACTIVE,
        })
        .then(schedules => Promise.all(schedules.map(s => s.execute())))
        .then(transactions => {
            console.log(transactions);
        });

}, null, true, 'America/Los_Angeles');
