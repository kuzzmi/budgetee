import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import I18n from './i18n';

I18n.init();

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
