import I18n from 'i18n-js';
import en from './assets/strings/en.json';
import ru from './assets/strings/ru.json';

I18n.defaultLocale = 'en';
I18n.fallbacks = true;

I18n.init = (lang) => {
    let locale = I18n.defaultLocale;
    if (window.navigator.languages) {
        locale = window.navigator.languages[0];
    } else {
        locale = window.navigator.userLanguage || window.navigator.language;
    }
    if (lang) {
        locale = lang;
    }

    I18n.locale = locale ? locale.replace(/_/, '-') : '';
};

I18n.translations = {
    en,
    ru,
};

export default I18n;
