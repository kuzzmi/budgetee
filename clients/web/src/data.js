import * as R from 'ramda';

export const isLoading = ({ loading }) =>
    !!loading;

export const isNotAsked = ({ loading, error, data }) =>
    !loading && !error && !data;

export const initData = () => ({
    loading: false,
    error: null,
    data: null,
});

export const setLoading = () => ({
    loading: true,
    error: null,
    data: null,
});

export const setError = error => ({
    loading: false,
    error,
    data: null,
});

export const setData = data => ({
    loading: false,
    error: null,
    data,
});

export const normalize = (data, field) =>
    R.indexBy(R.prop(field), data);

export const handleRequest = function(stateKey) {
    return promise => {
        this.setState(state => ({
            ...state,
            [stateKey]: setLoading(),
        }));

        promise.then(data => {
            this.setState(state => ({
                ...state,
                [stateKey]: setData(data),
            }));

            return data;
        }).catch(err => {
            this.setState(state => ({
                ...state,
                [stateKey]: setError(err),
            }));
        });
    };
};
