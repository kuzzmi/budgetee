const API_URL = 'http://127.0.0.1:3300';

const makeRequest = ({
    method = 'GET',
    version = 1,
    endpoint,
    data,
}) => {
    const options = {
        method,
    };

    if (!!data && (method !== 'GET' || method !== 'HEAD')) {
        options.headers = new Headers({
            'Content-Type': 'application/json',
        });
        options.body = JSON.stringify(data);
    }

    return fetch(
        `${ API_URL }/api/v${ version }/${ endpoint }`,
        options,
    ).then(response => response.json());
};

const API = {
    init: () => makeRequest({ endpoint: 'init' }),

    postAccount: data => makeRequest({
        method: 'POST',
        endpoint: 'accounts',
        data,
    }),

    putAccount: data => makeRequest({
        method: 'PUT',
        endpoint: 'accounts',
        data,
    }),

};

export default API;
