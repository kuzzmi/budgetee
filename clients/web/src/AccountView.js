import React, { Component } from 'react';
import {
    Segment,
    Checkbox,
    Header,
    Table,
    Popup,
    Button,
    Icon,
} from 'semantic-ui-react';

import c from './constants.js';
import { normalize } from './data.js';
import { format as formatCur } from './utils/currency.js';
import { format as formatDate } from './utils/date.js';

import EditAccount from './EditAccount.js';

import I18n from './i18n.js';

const Transactions = ({
    currencies,
    subcategories,
    transactions,
    payees,
}) => {
    const normalizedCurrencies = normalize(currencies, 'id');

    const TableHeader = () => (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell />
                <Table.HeaderCell>
                    { I18n.t('common.date') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('common.name') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('common.payee') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('common.category') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('common.expense') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('common.income') }
                </Table.HeaderCell>
                <Table.HeaderCell>
                    { I18n.t('transactionsView.cleared') }
                </Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    );

    const TransactionRow = ({ transaction: t }) => (
        <Table.Row>
            <Table.Cell collapsing>
                <Checkbox />
            </Table.Cell>
            <Table.Cell>
                { formatDate(t.date) }
            </Table.Cell>
            <Table.Cell warning={ !t.approved }>
                {
                    !t.approved &&
                    <Popup
                        hoverable
                        trigger={ <Icon name="attention" /> }>
                        <Header as="h4">
                            { I18n.t('transactionsView.notApprovedTitle') }
                        </Header>
                        { I18n.t('transactionsView.notApproved') }
                        <div style={{
                            marginTop: 15,
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'center',
                        }}>
                            <Button.Group>
                                <Button positive>
                                    { I18n.t('transactionsView.approveBtn') }
                                </Button>
                                <Button.Or text={ I18n.t('common.or') } />
                                <Button>
                                    { I18n.t('transactionsView.declineBtn') }
                                </Button>
                            </Button.Group>
                        </div>
                    </Popup>
                }
                { t.name }
            </Table.Cell>
            <Table.Cell>
                { payees[t.payee].name }
            </Table.Cell>
            <Table.Cell
                warning={ !t.subcategory }>
                {
                    t.subcategory ?
                        subcategories[t.subcategory].name :
                        <i>
                            <Icon name="attention" />
                            { I18n.t('transactionsView.noCategory') }
                        </i>
                }
            </Table.Cell>
            <Table.Cell>
                {
                    t.type === c.TRANSACTION_TYPE_EXPENSE &&
                    formatCur({
                        value: t.value,
                        symbol: normalizedCurrencies[t.currency].symbol,
                    })
                }
            </Table.Cell>
            <Table.Cell>
                {
                    t.type === c.TRANSACTION_TYPE_INCOME &&
                    formatCur({
                        value: t.value,
                        symbol: normalizedCurrencies[t.currency].symbol,
                    })
                }
            </Table.Cell>
            <Table.Cell>
                <Checkbox checked={ t.cleared } />
            </Table.Cell>
        </Table.Row>
    );

    return (
        <Table celled compact="very">
            <TableHeader />
            <Table.Body>
                {
                    transactions.map((t, i) => (
                        <TransactionRow transaction={ t } key={ i } />
                    ))
                }
            </Table.Body>
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                    <Table.HeaderCell>
                    </Table.HeaderCell>
                    <Table.HeaderCell>
                    </Table.HeaderCell>
                    <Table.HeaderCell />
                    <Table.HeaderCell />
                </Table.Row>
            </Table.Footer>
        </Table>
    );
};

export default class extends Component {
    state = {
        accountModal: null,
    };

    constructor(props) {
        super(props);

        this.openAccountModal = account => {
            this.setState(state => ({
                ...state,
                accountModal: account,
            }));
        };

        this.closeAccountModal = () => {
            this.setState(state => ({
                ...state,
                accountModal: null,
            }));
        };
    }

    render() {
        const {
            account,
            payees,
            subcategories,
            currencies,
            transactions,
        } = this.props;

        return (
            <Segment basic>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                    <Header as="h1" style={{ margin: 0, marginRight: 10 }}>
                        { account.name }
                    </Header>
                    <div>
                        <Button
                            compact
                            icon="pencil"
                            content={ I18n.t('sidebar.editAccount') }
                            onClick={ () => this.openAccountModal(account) }
                        />
                    </div>
                </div>

                <Transactions
                    payees={ payees }
                    subcategories={ subcategories }
                    currencies={ currencies }
                    transactions={ transactions }
                />

                <EditAccount
                    onNewAccount={ () => { /* */ } }
                    currencies={ currencies }
                    account={ this.state.accountModal }
                    close={ this.closeAccountModal }
                />
            </Segment>
        );
    }
}
