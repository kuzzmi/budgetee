import { leftpad } from './string.js';

export const format = ({
    value,
    symbol,
    prefix = true,
    spaced = false,
    comma = false,
}) => {
    const value_ = [ ...leftpad(value, 3, '0') ];
    value_.splice(-2, 0, '.');

    if (!symbol) {
        return value_.join('');
    };

    return `${ symbol } ${ value_.join('') }`;
};
