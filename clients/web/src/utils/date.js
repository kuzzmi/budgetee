import moment from 'moment';

export const format = date =>
    moment(date).format('L')

export const monthToDate = month => {
    const [ month_, year ] = month.split('-');

    return moment([ year, month_, '01' ].join('-'));
};
