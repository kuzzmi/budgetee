import React, { Component } from 'react';
import './App.css';
import {
    Dimmer,
    Loader,
} from 'semantic-ui-react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
} from 'react-router-dom';

import API from './api.js';
import {
    isLoading,
    isNotAsked,
    initData,
    handleRequest,
    normalize,
} from './data.js';

import Sidebar from './Sidebar.js';
import AccountView from './AccountView.js';
import BudgetView from './BudgetView.js';

import { groupBy } from 'ramda';

class App extends Component {
    state = {
        init: initData(),
    };

    constructor(props) {
        super(props);

        this.handleRequest = handleRequest.bind(this);

        this.onNewAccount = account => {
            const newAccounts = [].concat(this.state.init.data.accounts, account);

            this.setState(state => ({
                ...state,
                init: {
                    ...state.init,
                    data: {
                        ...state.init.data,
                        accounts: newAccounts,
                    },
                },
            }));
        };
    }

    componentDidMount() {
        this.handleRequest('init')(API.init());
    }

    render() {
        const {
            init,
        } = this.state;

        if (isLoading(init) || isNotAsked(init)) {
            return (
                <div className="App">
                    <Dimmer active={ isLoading(init) }>
                        <Loader />
                    </Dimmer>
                </div>
            );
        }

        const normalizedAccounts = normalize(init.data.accounts, 'id');
        const normalizedPayees = normalize(init.data.payees, 'id');
        const normalizedSubcategories = normalize(init.data.subcategories, 'id');
        const selectedAccount = normalizedAccounts[init.data.user.selectedAccount];
        const selectedMonth = init.data.user.selectedMonth;

        const transactionsByAccounts = groupBy(t => t.account)(init.data.transactions);
        const selectedAccountTransactions = transactionsByAccounts[selectedAccount.id];

        const subcategoriesByCategory = groupBy(sc => sc.category)(init.data.subcategories);
        const enhancedCategories = init.data.categories.map(c => ({
            ...c,
            subcategories: subcategoriesByCategory[c.id],
        }));

        return (
            <Router>
                <div className="App">
                    <Sidebar
                        data={ init.data }
                        onNewAccount={ this.onNewAccount }
                    />
                    <div style={{ marginLeft: 350 }}>
                        <Switch>
                            <Route path="/budget/" render={ props => (
                                <BudgetView
                                    categories={ enhancedCategories }
                                    currencies={ init.data.currencies }
                                    selectedMonth={ selectedMonth }
                                    { ...props }
                                />
                            )} />
                            <Route path="/accounts/" render={ props => (
                                <AccountView
                                    { ...props }
                                    payees={ normalizedPayees }
                                    subcategories={ normalizedSubcategories }
                                    currencies={ init.data.currencies }
                                    account={ selectedAccount }
                                    transactions={ selectedAccountTransactions }
                                />
                            )} />
                            <Redirect to="/budget/" />
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
