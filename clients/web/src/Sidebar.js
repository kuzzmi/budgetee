import React, { Component } from 'react';
import * as R from 'ramda';
import {
    Link,
} from 'react-router-dom';

import {
    Sidebar,
    Button,
    Menu,
    Icon,
    Label,
} from 'semantic-ui-react';

import EditAccount from './EditAccount.js';

import { format } from './utils/currency.js';
import I18n from './i18n.js';

import { normalize } from './data.js';

const Balances = ({ balances, currencies }) => {
    return balances.map(balance => (
        <Label
            key={ balance._id }
            size="small"
            color={ balance.value >= 0 ? 'green' : 'red' }
            style={{
                whiteSpace: 'nowrap',
            }}>
            {
                format({
                    value: balance.value,
                    symbol: currencies[balance.currency].symbol,
                })
            }
        </Label>
    ));
};

const Accounts = ({ accounts, currencies, selected }) => {
    return accounts.map(acc => (
        <Menu.Item
            key={ acc._id }
            name={ acc.name }
            active={ acc.id === selected }>
            <Link to="/accounts/">
                <div style={{
                    position: 'relative',
                    display: 'flex',
                    flexDirection: 'column',
                }}>
                    <span style={{ marginBottom: 10 }}>
                        { acc.name }
                    </span>
                    <div style={{
                        display: 'flex',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <Balances
                            balances={ acc.balances }
                            currencies={ currencies }
                        />
                    </div>
                </div>
            </Link>
        </Menu.Item>
    ));
};

export default class extends Component {
    state = {
        accountModal: null,
    };

    constructor(props) {
        super(props);

        this.openAccountModal = (account = true) => {
            this.setState(state => ({
                ...state,
                accountModal: account,
            }));
        };

        this.closeAccountModal = () => {
            this.setState(state => ({
                ...state,
                accountModal: null,
            }));
        };
    }

    render() {
        const {
            data,
            onNewAccount,
        } = this.props;

        console.log(data);

        const normalizedBudgets = normalize(data.budgets, 'id');
        const normalizedCurrencies = normalize(data.currencies, 'id');
        const selectedBudget = normalizedBudgets[data.user.selectedBudget];

        // Get all balances of all accounts, group them by currency, sum the values
        const allBalances =
            R.values(
                data.accounts.reduce((acc, cur) => ([
                    ...acc,
                    ...cur.balances,
                ]), []).reduce((acc, cur) => {
                    if (!acc[cur.currency]) {
                        acc[cur.currency] = cur;
                        return acc;
                    } else {
                        acc[cur.currency] = {
                            ...acc[cur.currency],
                            value: acc[cur.currency].value + cur.value,
                        };
                        return acc;
                    }
                }, {})
            );

        return (
            <Sidebar
                as={ Menu }
                width="wide"
                visible={ true }
                vertical
                inverted
                style={{
                    backgroundColor: '#072f58',
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'column',
                }}>

                <Menu.Item>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                    }}>
                        <big>
                            <b>{ selectedBudget.name }</b>
                        </big>
                    </div>
                </Menu.Item>

                <Menu.Item>
                    <Link to="/budget/">
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <Icon name="tags" size="big" />
                            <big style={{ marginLeft: 10 }}>
                                <b>{ I18n.t('sidebar.budget') }</b>
                            </big>
                        </div>
                    </Link>
                </Menu.Item>

                <Menu.Item>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                    }}>
                        <Icon name="line chart" size="big" />
                        <big style={{ marginLeft: 10 }}>
                            <b>{ I18n.t('sidebar.stats') }</b>
                        </big>
                    </div>
                </Menu.Item>

                <Menu.Item>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                    }}>
                        <Icon name="money" size="big" />
                        <big style={{ marginLeft: 10 }}>
                            <b>{ I18n.t('sidebar.allAccounts') }</b>
                        </big>
                    </div>
                </Menu.Item>

                <div style={{ height: 40 }} />

                <Menu.Item>
                    <Menu.Header>
                        { I18n.t('sidebar.accounts') }
                    </Menu.Header>
                    <Accounts
                        accounts={ data.accounts }
                        selected={ data.user.selectedAccount }
                        currencies={ normalizedCurrencies }
                    />
                    <Menu.Item>
                        <Button
                            size="mini"
                            compact
                            onClick={ () => this.openAccountModal() }
                            style={{
                                color: 'white',
                                backgroundColor: 'rgba(255,255,255,.15)',
                            }}>
                            <Icon name="plus" size="small" />
                            { I18n.t('sidebar.newAccount') }
                        </Button>
                    </Menu.Item>
                </Menu.Item>

                <div style={{ display: 'flex', flex: 1 }} />

                <Menu.Item
                    name="total">
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <span style={{ marginBottom: 10 }}>
                            { I18n.t('common.total') }
                        </span>
                        <div style={{
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            <Balances
                                balances={ allBalances }
                                currencies={ normalizedCurrencies }
                            />
                        </div>
                    </div>
                </Menu.Item>

                <EditAccount
                    onNewAccount={ onNewAccount }
                    currencies={ data.currencies }
                    account={ this.state.accountModal }
                    close={ this.closeAccountModal }
                />

            </Sidebar>
        );
    }
}
