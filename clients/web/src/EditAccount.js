import React, { Component } from 'react';
import {
    Modal,
    Button,
    Form,
} from 'semantic-ui-react';

import I18n from './i18n.js';

import API from './api.js';

const Balances = ({
    startingBalances,
    currencyOptions,
    update,
    remove,
}) =>
    startingBalances.map((sb, i) => (
        <Form.Group key={ i }>
            <Form.Dropdown
                name="currency"
                search
                selection
                value={ sb.currency }
                disabled={ !!sb._id }
                placeholder={
                    I18n.t('common.select', {
                        subject: I18n.t('common.currency'),
                    })
                }
                width={ 10 }
                options={ currencyOptions }
                onChange={ update(i) }
            />
            <Form.Input
                name="value"
                placeholder="0.00"
                value={ sb.value }
                disabled={ !!sb._id }
                width={ 3 }
                onChange={ update(i) }
            />
            <Button
                icon="trash"
                disabled={ !!sb._id }
                onClick={ () => remove(i) }
            />
        </Form.Group>
    ));


export default class extends Component {
    state = {
        name: '',
        startingBalances: [{
            value: '',
            currency: null,
        }],
    };

    constructor(props) {
        super(props);

        this.updateName = (e, { value }) => {
            this.setState(state => ({
                ...state,
                name: value,
            }));
        };

        this.addStartingBalance = () => {
            this.setState(state => ({
                ...state,
                startingBalances: [
                    ...state.startingBalances,
                    {
                        value: '',
                        currency: null,
                    },
                ],
            }));
        };

        this.updateStartingBalance = index => (e, { name, value }) => {
            console.log(name, value);
            this.setState(state => ({
                ...state,
                startingBalances: state.startingBalances.map((sb, i) => {
                    if (i === index) {
                        return {
                            ...sb,
                            [ name ]: value,
                        };
                    };
                    return sb;
                }),
            }));
        };

        this.removeStartingBalance = index => {
            this.setState(state => ({
                ...state,
                startingBalances: state.startingBalances.filter((_, i) => i !== index),
            }));
        };

        this.save = () => {
            API.postAccount({
                name: this.state.name,
                balances: this.state.startingBalances,
            }).then(data => {
                this.props.onNewAccount(data);
                this.props.close();
            });
        };
    }

    componentWillReceiveProps({ account }) {
        console.log(account && account.balances);
        this.setState(state => ({
            ...state,
            name: account ? account.name : '',
            startingBalances: account ? account.balances : '',
        }));
    }

    render() {
        const {
            account,
            currencies,
            close,
        } = this.props;

        const {
            startingBalances,
        } = this.state;

        const currencyOptions =
            currencies.map(c => ({
                key: c.id,
                value: c.id,
                text: `${ c.symbol } - ${ c.name }`,
            }));

        if (account === null) {
            return null;
        }

        return (
            <Modal
                open={ !!account }
                size="tiny"
                dimmer={ true }
                onClose={ close }>
                <Modal.Header>
                    {
                        (typeof account === 'object') ?
                            I18n.t('editAccount.titleEdit', { name: account.name }) :
                            I18n.t('editAccount.titleNew')
                    }
                </Modal.Header>
                <Modal.Content>
                    <p>{ I18n.t('editAccount.description') }</p>

                    <Form>
                        <Form.Input
                            fluid
                            value={ this.state.name }
                            label={ I18n.t('common.name') }
                            placeholder={ I18n.t('common.typeHere') }
                            onChange={ this.updateName }
                        />

                        <Form.Field>
                            <label>{ I18n.t('editAccount.startingBalances') }</label>
                            <div style={{ marginBottom: 10 }}>
                                <small>{ I18n.t('editAccount.startingBalancesTip') }</small>
                            </div>

                            <Balances
                                startingBalances={ startingBalances }
                                currencyOptions={ currencyOptions }
                                update={ this.updateStartingBalance }
                                remove={ this.removeStartingBalance }
                            />

                            <Button
                                fluid
                                size="tiny"
                                color="blue"
                                content={ I18n.t('editAccount.addAnother') }
                                onClick={ this.addStartingBalance }
                            />
                        </Form.Field>
                    </Form>
                </Modal.Content>

                <Modal.Actions>
                    <Button
                        color="black"
                        content={ I18n.t('common.cancel') }
                        floated="left"
                        onClick={ close }
                    />
                    <Button
                        positive
                        icon="checkmark"
                        labelPosition="right"
                        content={ I18n.t('common.save') }
                        onClick={ this.save }
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}
