import React, { Component } from 'react';
import {
    Segment,
    Table,
    Header,
    Label,
    Grid,
    Form,
    Popup,
    Button,
} from 'semantic-ui-react';

import { groupBy } from 'ramda';

import { monthToDate } from './utils/date.js';
import { format as formatCur } from './utils/currency.js';

import I18n from './i18n.js';

const Subcategory = ({ sc, currencies, month }) => {
    const monthlyBudget = groupBy(mb => mb.month, sc.monthlyBudgets)[month];
    let monthlyBudgetByCurrency;

    if (monthlyBudget) {
        monthlyBudgetByCurrency = groupBy(b => b.currency, monthlyBudget[0].balances);
    }

    return (
        <Table.Row key={ sc.id }>
            <Table.Cell>
                { sc.name }
            </Table.Cell>
            {
                currencies.map((cur, i) => {
                    const value =
                        monthlyBudgetByCurrency &&
                        monthlyBudgetByCurrency[cur.id] ?
                        monthlyBudgetByCurrency[cur.id][0].value :
                        0;

                    return (
                        <Table.Cell key={ i } textAlign="center">
                            <Popup
                                flowing
                                position='bottom center'
                                on={['click']}
                                trigger={
                                    <Label
                                        content={ formatCur({ value }) }
                                        style={{
                                            cursor: 'pointer',
                                            color: value === 0 && '#acacac',
                                            fontWeight: value !== 0 ? 700 : 500,
                                            backgroundColor: 'transparent',
                                        }}
                                    />
                                }>
                                <Form style={{ marginBottom: 5 }}>
                                    <Form.Input
                                        fluid
                                        value={ value }
                                        label="Update budget"
                                    />
                                </Form>
                                <Button positive fluid size="small">
                                    { I18n.t('common.ok') }
                                </Button>
                            </Popup>
                        </Table.Cell>
                    );
                })
            }
            {
                currencies.map((cur, i) => {
                    const value =
                        monthlyBudgetByCurrency &&
                        monthlyBudgetByCurrency[cur.id] ?
                        monthlyBudgetByCurrency[cur.id][0].activity :
                        0;

                    return (
                        <Table.Cell key={ i } textAlign="center">
                            <Label
                                content={ formatCur({ value }) }
                                style={{
                                    color: value === 0 && '#cecece',
                                    fontWeight: value !== 0 ? 700 : 500,
                                    backgroundColor: 'transparent',
                                }}
                            />
                        </Table.Cell>
                    );
                })
            }
            {
                currencies.map((cur, i) => {
                    const activityValue =
                        monthlyBudgetByCurrency &&
                        monthlyBudgetByCurrency[cur.id] ?
                        monthlyBudgetByCurrency[cur.id][0].activity :
                        0;
                    const budgetedValue =
                        monthlyBudgetByCurrency &&
                        monthlyBudgetByCurrency[cur.id] ?
                        monthlyBudgetByCurrency[cur.id][0].value :
                        0;
                    const value = budgetedValue - activityValue;

                    return (
                        <Table.Cell key={ i } textAlign="center">
                            <Label
                                color={ value > 0 ? 'green' : 'default' }
                                content={ formatCur({ value }) }
                                style={{
                                    color: value === 0 && '#cecece',
                                    backgroundColor: value === 0 && 'transparent',
                                }}
                            />
                        </Table.Cell>
                    );
                })
            }
        </Table.Row>
    );
};

const Category = ({ category, currencies, month }) => {
    return [
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell colSpan={ currencies.length * 3 + 1 }>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <span>{ category.name }</span>
                        <Button
                            compact
                            size="mini"
                            icon="plus"
                            style={{ marginLeft: 10 }}
                        />
                    </div>
                </Table.HeaderCell>
            </Table.Row>
        </Table.Header>,
        <Table.Body>
            {
                category.subcategories.map(sc => (
                    <Subcategory
                        sc={ sc }
                        key={ sc.id }
                        month={ month }
                        currencies={ currencies }
                    />
                ))
            }
        </Table.Body>
    ];
};

const Categories = ({ categories, currencies, month }) => {
    const currencyHeaderCells = currencies.map((cur, i) => (
        <Table.HeaderCell collapsing key={ i }>
            { cur.symbol }
        </Table.HeaderCell>
    ));

    return (
        <Table celled compact="very" selectable>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell rowSpan={ 2 }>
                        { I18n.t('common.category') }
                    </Table.HeaderCell>
                    <Table.HeaderCell
                        colSpan={ currencies.length }
                        style={{ textAlign: 'center' }}>
                        { I18n.t('budgetView.budgeted') }
                    </Table.HeaderCell>
                    <Table.HeaderCell
                        colSpan={ currencies.length }
                        style={{ textAlign: 'center' }}>
                        { I18n.t('budgetView.activity') }
                    </Table.HeaderCell>
                    <Table.HeaderCell
                        colSpan={ currencies.length }
                        style={{ textAlign: 'center' }}>
                        { I18n.t('budgetView.available') }
                    </Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                    { currencyHeaderCells }
                    { currencyHeaderCells }
                    { currencyHeaderCells }
                </Table.Row>
            </Table.Header>
            {
                categories.map(c => (
                    <Category
                        key={ c.id }
                        category={ c }
                        currencies={ currencies }
                        month={ month }
                    />
                ))
            }
        </Table>
    );
};

const TopBar = ({ month }) => {
    const date = monthToDate(month);

    return (
        <div>
            <Header as="h1">{ date.format('MMM Y') }</Header>
        </div>
    );
};

export default class extends Component {
    render() {
        const {
            categories,
            currencies,
            selectedMonth,
        } = this.props;

        return (
            <div>
                <TopBar
                    month={ selectedMonth }
                />
                <Segment basic>
                    <Categories
                        categories={ categories }
                        currencies={ currencies }
                        month={ selectedMonth }
                    />
                </Segment>
            </div>
        );
    }
}

